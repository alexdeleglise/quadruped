import argparse
import math
import numpy as np
import pybullet as p
from time import sleep

dt = 0.01



def init():
    """Initialise le simulateur

    Returns:
        int -- l'id du robot
    """
    # Instanciation de Bullet
    physicsClient = p.connect(p.GUI)
    p.setGravity(0, 0, -10)

    # Chargement du sol
    planeId = p.loadURDF('plane.urdf')

    # Chargement du robot
    startPos = [0, 0, 0.1]
    startOrientation = p.getQuaternionFromEuler([0, 0, 0])
    robot = p.loadURDF("./quadruped/robot.urdf",
                        startPos, startOrientation)

    p.setPhysicsEngineParameter(fixedTimeStep=dt)
    return robot

def setJoints(robot, joints):
    """Définis les angles cibles pour les moteurs du robot

    Arguments:
        int -- identifiant du robot
        joints {list} -- liste des positions cibles (rad)
    """
    jointsMap = [0, 1, 2, 4, 5, 6, 8, 9, 10, 12, 13, 14]
    for k in range(len(joints)):
        jointInfo = p.getJointInfo(robot, jointsMap[k])
        p.setJointMotorControl2(robot, jointInfo[0], p.POSITION_CONTROL, joints[k])

def demo(t, amplitude):
    """Démonstration de mouvement (fait osciller une patte)

    Arguments:
        t {float} -- Temps écoulé depuis le début de la simulation

    Returns:
        list -- les 12 positions cibles (radian) pour les moteurs
        float -- amplitude de l'oscillation
    """
    joints = [0]*12
    joints[0] = math.sin(t) * amplitude
    return joints


def interpolate(xs, ys, x):
    """ Donne une interpolation pour une abscisse donnée

    Arguments :
        xs -- liste des valeurs en abscisse
        ys -- liste des valeurs en ordonnée
        x -- la valeur d'abscisse dont on veut avoir l'interpolation

    Returns :
        valeur d'ordonnée par interpolation de la valeur x

    """
    if x<=xs[0]:
        return ys[0];
    if x>=xs[-1]:
        return ys[-1];
    for i in range(len(xs)):
        if x<xs[i]:
            return ((ys[i]-ys[i-1])/(xs[i]-xs[i-1]))*(x-xs[i])+ys[i]

def inverse(x,y,z):
    """applique la cinématique inverse pour trouver les angles correspondant à une position donnée

    Arguments :
        x, y, z {float} -- correspond à la position voulue ( en m ) pour le bout de la patte

    Returns :
        list -- liste des 3 angles pour les articulations de la patte
    """
    T=[]
    l1=0.045
    l2=0.065
    l3=0.087
    t0=math.atan2(y,x)
    T.append(t0)
    d=math.sqrt((x-l1*math.cos(t0))**2+(y-l1*math.sin(t0))**2+z**2)
    if d == 0 :
        d = 0.0001
    L=[]
    L.append((l2**2+d**2-l3**2)/(2*d*l2))
    L.append(z/d)
    L.append((l2**2+l3**2-d**2)/(2*l2*l3))
    for i in range (len(L)) :
        if L[i] < -1 :
            L[i]=-1
        if L[i] > 1 :
            L[i] = 1
    T.append(math.acos(L[0])+math.asin(L[1]))
    T.append((-math.acos(L[2]))-math.pi)
    return T

def leg_ik():
    """Permet de controler les positions des pattes du robot grâce aux sliders

    Returns:
        list -- les angles correspondant aux positions données par les sliders
    """
    joints = [0]*12
    joints[0:3]=inverse(p.readUserDebugParameter(x1),p.readUserDebugParameter(y1),p.readUserDebugParameter(z1))
    joints[3:6]=inverse(p.readUserDebugParameter(x2),p.readUserDebugParameter(y2),p.readUserDebugParameter(z2))
    joints[6:9]=inverse(p.readUserDebugParameter(x3),p.readUserDebugParameter(y3),p.readUserDebugParameter(z3))
    joints[9:12]=inverse(p.readUserDebugParameter(x4),p.readUserDebugParameter(y4),p.readUserDebugParameter(z4))
    return joints


def deplacement_patte(i,t,DEP,TPS,joints):
    """Utilise l'interpolation pour faire le parcours voulu

    Arguments :
        i {int} -- numéro de la patte à déplacer
        t {float} -- temps
        DEP {list-list-float} -- Liste de liste des positions ( en m ) du parcours : DEP[0] liste des x
                                                                                   DEP[1] liste des y
                                                                                   DEP[2] liste des z
        TPS {list-float} -- Liste des temps pour chaque position
        joints {list} -- liste des angles avant le mouvement

    Returns :
        list -- liste des angles correspondant au temps t
    """
    t=math.fmod(t,TPS[-1])
    newx=interpolate(TPS,DEP[0],t)
    newy=interpolate(TPS,DEP[1],t)
    newz=interpolate(TPS,DEP[2],t)
    joints[0+i*3:3+i*3]=inverse(newx,newy,newz)
    return joints


def immobile(t):
    """mets le robot en position stable

    Arguments :
        t {int} -- temps

    Returns :
        list -- angles des pattes correpondant au temps t
    """
    joints=[0]*12
    for i in range (4) :
        joints[i*3:3+i*3] = deplacement_patte(i,t,[[0.2,0.09,0.09],[0,0,0],[0,-0.05,-0.05]],[0,2,1000],joints)[i*3:3+i*3]
    return joints



def computeleg_ik(idleg,x,y,z,theta_extra=0,default_x=0.1,default_y=0,default_z=-0.05) :
    """Donne les positions d'une patte pour une position donnée du robot

    Arguments :
        idleg {int} -- numéro de la patte
        x,y,z {float} -- position du robot voulue
        theta_extra {float} -- orientation du robot voulue
        default_x,default_y,default_z {float} -- position stable de la patte

    Returns :
        floats -- les positions de la patte
    """
    theta = idleg*math.pi/2 + math.pi/4
    newx= default_x*math.cos(theta_extra) +default_y*math.sin(theta_extra) - x*math.cos(theta) - y*math.sin(theta)
    newy = default_y*math.cos(theta_extra) - default_x*math.sin(theta_extra) - y*math.cos(theta) + x*math.sin(theta)
    newz= default_z - z
    return newx,newy,newz



def robot_ik():
    """Permet de controler les positions du robot grâce aux sliders

    Returns :
        list -- angles des pattes correspondant
    """
    joints = [0]*12
    for i in range (4) :
        a,b,c = computeleg_ik(i,p.readUserDebugParameter(x),p.readUserDebugParameter(y),p.readUserDebugParameter(z))
        joints[i*3:3+i*3] = inverse(a,b,c)
    return joints

def walk(t,xspd,yspd,tspd,v=1):
    """Met le robot en marche

    Arguments :
        t {float} -- temps
        xspd {float} -- vitesse en m/s de la marche selon x
        yspd {float} -- vitesse en m/s de la marche selon y
        tspd {float} -- vitesse de rotation en rad/s du robot
        v {float} -- facteur de vitesse de mouvement du robot

    Returns :
        list -- Angles des pattes correpondant au temps t
    """
    TPS=[0,0.5*v,1.5*v,2*v,3*v]
    newx,newy,newz=[0]*4,[0]*4,[0]*4
    depx = xspd*3*v/4
    depy = yspd*3*v/4
    for i in [0,2] :
        x=[0]*5
        y=[0]*5
        z=[0]*5
        x[0],y[0],z[0] = computeleg_ik(i,-depx,-depy,-0.005,-tspd*0.5*v)
        x[1],y[1],z[1] = computeleg_ik(i,-depx,-depy,0.005,-tspd*1.5*v)
        x[2],y[2],z[2] = computeleg_ik(i,depx,depy,0.005,tspd*0.5*v)
        x[3],y[3],z[3] = computeleg_ik(i,depx,depy,-0.005,tspd*1.5*v)
        x[4],y[4],z[4] = x[0],y[0],z[0]
        newx[i]=x
        newy[i]=y
        newz[i]=z
    for i in [1,3] :
        x=[0]*5
        y=[0]*5
        z=[0]*5
        x[0],y[0],z[0] = computeleg_ik(i,depx,depy,0.005,tspd*0.5*v)
        x[1],y[1],z[1] = computeleg_ik(i,depx,depy,-0.005,tspd*1.5*v)
        x[2],y[2],z[2] = computeleg_ik(i,-depx,-depy,-0.005,-tspd*1*v)
        x[3],y[3],z[3] = computeleg_ik(i,-depx,-depy,0.005,-tspd*1.5*v)
        x[4],y[4],z[4] = x[0],y[0],z[0]
        newx[i]=x
        newy[i]=y
        newz[i]=z
    joints = [0]*12
    for i in range (4) :
        joints[i*3:3+i*3] = deplacement_patte(i,t,[newx[i],newy[i],newz[i]],TPS,joints)[i*3:3+i*3]
    return joints



def goto(t,x,y,th,spd=0.1,tspd=math.pi/6,velocity=0.5) :
    """Met en marche le robot jusqu'à une position

    Arguments :
        t {int} -- Temps
        x,y {float} -- position (en m) cible du robot
        th {float} -- orientation (en rad) cible du robot
        spd {float} -- vitesse (en m/s) de marche du robot
        tspd {float} -- vitesse de rotation (en rad/s) du robot
        velocity {float} -- facteur de vitesse de mouvement du robot
    Returns :
        list -- Angles des pattes correspondant au temps t
    """
    dist = math.sqrt(x**2+y**2)
    if dist > 0 :
        xspd = (x/dist)*spd
        yspd = (y/dist)*spd
    if t < dist/spd :
        return walk(t,xspd,yspd,0,velocity)
    elif t< (dist/spd + th/tspd) :
        return walk(t,0,0,tspd,velocity)
    else :
        return immobile(t)


def fun(t,joints) :
    TPS=[0,3]
    x1=0.1
    z1=-0.05
    x2=0.03
    x3=0.01
    z2=-0.195
    if t<3 :
        for i in [1,3] :
            joints[i*3:3+i*3] = deplacement_patte(i,t,[[x1,x2],[0,0],[z1,z2]],TPS,joints)[i*3:3+i*3]
        for i in [0,2] :
            joints[i*3:3+i*3] = deplacement_patte(i,t,[[x1,x3],[0,-0.2],[z1,0]],TPS,joints)[i*3:3+i*3]
        return joints
    else :
        t=math.fmod(t,0.75)
        joints[0:3] = deplacement_patte(0,t,[[x3,0.19,x3],[-0.2,0,0.20],[0,0,0]],[0,0.375,0.75],joints)[0:3]
        joints[6:9] =deplacement_patte(2,t,[[x3,0.19,x3],[-0.2,0,0.20],[0,0,0]],[0,0.375,0.75],joints)[6:9]
    return joints


def autocollision(joints):
    """ Empeche la collision des pattes entre elles
    """
    for i in [0,1,2,3]:
        if (joints[i*3] > math.pi/2 + joints[int(math.fmod((i+1)*3,12))] ):
            joints[i*3] = math.pi/2 + joints[int(math.fmod((i+1)*3,12))]
    return(joints)


if __name__ == "__main__":
    # Arguments
    parser = argparse.ArgumentParser(prog="TD Robotique S8")
    parser.add_argument('-m', type=str, help='Mode', required=True)
    parser.add_argument('-x', type=float, help='X target for goto (m)', default=1.0)
    parser.add_argument('-y', type=float, help='Y target for goto (m)', default=0.0)
    parser.add_argument('-t', type=float, help='Theta target for goto (rad)', default=0.0)
    args = parser.parse_args()

    mode, x, y, th = args.m, args.x, args.y, args.t

    if mode not in ['demo', 'leg_ik', 'robot_ik','walk', 'goto', 'fun','immobile']:
        print('Le mode %s est inconnu' % mode)
        exit(1)

    robot = init()
    if mode == 'demo':
        amplitude = p.addUserDebugParameter("amplitude", 0.1, 1, 0.3)
        print('Mode de démonstration...')
    elif mode == 'leg_ik' :
        x1 = p.addUserDebugParameter("x1", 0.03, 0.197, 0.09)
        y1 = p.addUserDebugParameter("y1", -0.20, 0.20, 0)
        z1 = p.addUserDebugParameter("z1", -0.20, 0.20, -0.05)
        x2 = p.addUserDebugParameter("x2", 0.03, 0.197, 0.09)
        y2 = p.addUserDebugParameter("y2", -0.20, 0.20, 0)
        z2 = p.addUserDebugParameter("z2", -0.20, 0.20, -0.05)
        x3 = p.addUserDebugParameter("x3", 0.03, 0.197, 0.09)
        y3 = p.addUserDebugParameter("y3", -0.20, 0.20, 0)
        z3 = p.addUserDebugParameter("z3", -0.20, 0.20, -0.05)
        x4 = p.addUserDebugParameter("x4", 0.03, 0.197, 0.09)
        y4 = p.addUserDebugParameter("y4",-0.20, 0.20, 0)
        z4 = p.addUserDebugParameter("z4", -0.20, 0.20, -0.05)
        print('Mode de deplacement des pattes...')
    elif mode == 'immobile' :
        print('Mode immobile...')
    elif mode == 'robot_ik' :
        x = p.addUserDebugParameter("x", -0.10, 0.10, 0)
        y = p.addUserDebugParameter("y", -0.10, 0.10, 0)
        z = p.addUserDebugParameter("z", -0.20, 0.20, 0)
        print('Mode de déplacement du robot')
    elif mode == 'walk' :
        x_spd = p.addUserDebugParameter("x_spd", -0.3, 0.3, 0)
        y_spd = p.addUserDebugParameter("y_spd", -0.3, 0.3, 0)
        t_spd = p.addUserDebugParameter("t_spd", -math.pi/2, math.pi/2, 0)
        v=p.addUserDebugParameter("velocity",0.1,1.5,0.5)
        print('mode de marche...')
    elif mode == 'goto' :
        x, y, th = args.x, args.y, args.t
    elif mode == 'fun' :
        print('mode divertissant...')
    else:
        raise Exception('Mode non implémenté: %s' % mode)

    t = 0

    # Boucle principale
    while True:
        t += dt

        if mode == 'demo':
            # Récupération des positions cibles
            joints = demo(t, p.readUserDebugParameter(amplitude))

        if mode == 'leg_ik':
            joints = leg_ik()
        if mode == 'parcours_patte' :
            if t<2 :
                joints = immobile(t)
            else :
                joints = deplacement_patte(0,t-2,DEP,T,joints)
        if mode == 'immobile' :
            joints = immobile(t)
        if mode == 'robot_ik' :
            if t<2 :
                joints = immobile(t)
            else :
                joints = robot_ik()
        if mode == 'walk' :
            if t<2 :
                joints = immobile(t)
            else :
                joints = walk(t-2,p.readUserDebugParameter(x_spd),p.readUserDebugParameter(y_spd),p.readUserDebugParameter(t_spd),p.readUserDebugParameter(v))

        if mode == 'goto' :
            if t<2 :
                joints = immobile(t)
            else :
                joints = goto(t-2,x,y,th)
        if mode == 'fun' :
            if t<2 :
                joints = immobile(t)
            else :
                joints = fun(t-2,joints)

        joints=autocollision(joints)
        joints2 =[0] *12
        joints2[9:12] = joints[0:3]
        joints2[0:9] = joints[3:]

        # Envoi des positions cibles au simulateur
        setJoints(robot, joints2)

        # Mise à jour de la simulation
        p.stepSimulation()
        sleep(dt)
